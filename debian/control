Source: libmaxmind-db-reader-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Florian Schlichting <fsfs@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 12)
Build-Depends-Indep: libdata-ieee754-perl <!nocheck>,
                     libdata-printer-perl <!nocheck>,
                     libdata-validate-ip-perl <!nocheck>,
                     libdatetime-perl <!nocheck>,
                     liblist-allutils-perl <!nocheck>,
                     libmaxmind-db-common-perl <!nocheck>,
                     libmodule-implementation-perl <!nocheck>,
                     libmoo-perl <!nocheck>,
                     libmoox-strictconstructor-perl <!nocheck>,
                     libnamespace-autoclean-perl <!nocheck>,
                     libpath-class-perl <!nocheck>,
                     librole-tiny-perl <!nocheck>,
                     libscalar-list-utils-perl <!nocheck>,
                     libtest-bits-perl <!nocheck>,
                     libtest-fatal-perl <!nocheck>,
                     libtest-number-delta-perl <!nocheck>,
                     libtest-requires-perl <!nocheck>,
                     perl
Standards-Version: 4.5.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libmaxmind-db-reader-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libmaxmind-db-reader-perl.git
Homepage: https://metacpan.org/release/MaxMind-DB-Reader

Package: libmaxmind-db-reader-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libdata-ieee754-perl,
         libdata-printer-perl,
         libdata-validate-ip-perl,
         libdatetime-perl,
         liblist-allutils-perl,
         libmaxmind-db-common-perl,
         libmodule-implementation-perl,
         libmoo-perl,
         libmoox-strictconstructor-perl,
         libnamespace-autoclean-perl,
         librole-tiny-perl
Recommends: libmaxmind-db-reader-xs-perl
Description: Perl module to read MaxMind DB files and look up IP addresses
 MaxMind::DB::Reader provides a low-level interface to the MaxMind DB
 file format as described at https://maxmind.github.io/MaxMind-DB/.
 .
 If you are looking for an interface to MaxMind's GeoIP2 or GeoLite2
 downloadable databases, you should also check out the libgeoip2-perl
 package, which provides a higher level OO interface to those databases.
 .
 The MaxMind-DB-Reader distribution ships with a single pure Perl
 implementation of the Reader API. There is a separate distribution that
 provides an XS implementation, which links against libmaxminddb. It is
 packaged as libmaxmind-db-reader-xs-perl and approximately 100 times
 faster than the pure Perl implementation.
 .
 This module is deprecated and will only receive fixes for major bugs and
 security vulnerabilities. New features and functionality will not be added.
